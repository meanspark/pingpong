class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  
  def bonjour
    render text: "Hej, värld! God morgon. Hur mår du?"
  end
end
